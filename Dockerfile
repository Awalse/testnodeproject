# Use Microsoft's official build .NET image. #โหลด Image สำหรับตัวบิลล์ as คือตั้งชื่อ
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build 
WORKDIR /app

# Install production dependencies.
# Copy csproj and restore as distinct layers.
COPY *.csproj ./
RUN dotnet restore

# Copy local code to the container image.
COPY . ./
WORKDIR /app

# Build a release artifact.
RUN dotnet publish -c Release -o out


# Use Microsoft's official runtime .NET image. #โหลด environtment ที่ใช้ run serve หลังจากด้านบน
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS runtime
WORKDIR /app
COPY --from=build /app/out ./ 

# port
EXPOSE 80 

# healthCheck
HEALTHCHECK  --interval=15s --timeout=3s \
  CMD wget --no-verbose --tries=1 --spider http://localhost/ || exit 1

# Run the web service on container startup.
ENTRYPOINT ["dotnet", "helloworld-csharp.dll"]
